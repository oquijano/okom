
update_pos:
	xgettext --from-code=UTF-8 -L JavaScript --sort-output -o static/po/exercise.pot static/exercise.js 
	find  static/po/* -type d -exec   msgmerge --update {}/exercise.po  static/po/exercise.pot \;

update_locale_json:
	find . -name exercise.po  | xargs  -I %  sh -c 'po2json % `echo % | sed  s/\\\.po/\\\.json/`'



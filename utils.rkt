;; 
;; Okom - A platform of supervised cognitive exercises
;; Copyright (C) 2023 Oscar Alberto Quijano Xacur
;;
;; This file is part of Okom.
;;
;; Okom is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; Okom is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Okom.  If not, see <http://www.gnu.org/licenses/>.
;; 
#lang racket/base

(require web-server/http/response-structs
	 racket/list
	 racket/path
	 racket/file
	 racket/string
	 )

(require (for-syntax racket/base
		     racket/string
		     syntax/parse
		     ))




(provide hset!
	 href
	 template-response
	 require-define-template-function
	 adapt-svg-stroke)

;; The following macro receives a predicate and a variable
;; The predicate is assumed to end with ?
;; If the variable satisfies the predicate, the value of the variable is returned
;; Otherwise an exception is raised saying the variable must be of the specified type.
;; The ending ? is removed from the predicate in the error message
(define-syntax (type-or-exception stx)
  (syntax-case stx ()
    [(_ predicate variable)
     (with-syntax ([type (regexp-replace #rx"\\?$"  (symbol->string (syntax->datum #'predicate)) "")])
       #'(if (predicate variable)
	     variable
	     (error (format "~a must be a ~a" 'variable type))))]))

(define-syntax (template-response stx)
  (syntax-case stx ()
    [(_ template-expr)
     #'(response/full
	200 #"Okay"
	(current-seconds) TEXT/HTML-MIME-TYPE
	empty
	(list (string->bytes/utf-8
	       template-expr)))
     ]))

(define-syntax (require-define-template-function stx)
  (syntax-parse stx
		[(_)
		 (with-syntax ([define-template-function (datum->syntax stx 'define-template-function)])
		   #'(begin
		       (define-namespace-anchor a)
		       (define ns (namespace-anchor->namespace a))
		       (define-syntax (define-template-function stx)
			 (... (syntax-parse stx
					    [(define-template-function <fname>:id (<args>:id ...)  <template-filename>:string (<varname>:id <expr>) ... )
					     #'(define <fname> (make-parameter (void)
									       (lambda (a-path)
										 (unless (path-string? a-path )
										   (error "The value must be a path string"))
										 (define old-current-directory (current-directory))
										 (current-directory a-path)
										 (define returnf (eval `(lambda (<args> ...)
													  (let  ( [<varname> <expr>]...)
													    (template-response (include-template <template-filename> )))) ns ))
										 (current-directory old-current-directory)
										 returnf)))])))))]))

(define (hset-aux a-symbols-list value)
  (let ret ([remaining-list a-symbols-list]
	    [accum (car a-symbols-list)])
    
    (let ([current-element (car remaining-list)]
	  [next-element-list (cdr remaining-list)])
      
      (if (null? (cdr next-element-list))
	  (hash-set! accum  (car next-element-list)  value)
	  (ret next-element-list (hash-ref accum (car next-element-list))  ))) ))

(define (href-aux a-symbols-list)
  (let ret ([remaining-list (cdr a-symbols-list)]
	    [accum (car a-symbols-list)])    
    (if (null? remaining-list)
	accum 
	(ret
	 (cdr remaining-list)
	 (hash-ref accum (car remaining-list))))))

(define-syntax (hset! stx)
  (syntax-case stx ()
    [(_ (elems ...) value)
     #'(hset-aux (elems ...) value)
     ]
    [(_ doted-symbols value)
     (let ([elements (string-split (symbol->string (syntax->datum #'doted-symbols)) ".") ])
       (if (null? (cdr elements ))
	   #'(hset-aux doted-symbols value)
	  (let  ([symbols-list (map string->symbol  elements ) ])
	    (with-syntax ([(symbol0 symbols ...)  (datum->syntax #'doted-symbols symbols-list )  ])       
	      #`(hset-aux (list symbol0  'symbols ...) value   )
	      ))))
     ]))

(define-syntax (href stx)
  (syntax-case stx ()
    [(_ (elems ...))
     #'(href-aux (elems ...))
     ]
    [(_ doted-symbols)
     (let ([elements (string-split (symbol->string (syntax->datum #'doted-symbols)) ".") ])
       (if (null? (cdr elements))
	   #'doted-symbols
	   (let  ([symbols-list (map string->symbol  elements ) ])
	    (with-syntax ([(symbol0 symbols ...)  (datum->syntax #'doted-symbols symbols-list )  ])
	      #`(href-aux (list symbol0  'symbols ...))
	      ))))]))

(define (adapt-svg-stroke path)
  (define file-contents (file->string path))
  (string-replace
   (string-replace
    file-contents
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    "")
   "stroke:rgb(0%,0%,0%)"
   "stroke:currentColor")
  )


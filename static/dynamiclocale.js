/*
  Copyright 2023 Oscar Alberto Quijano Xacur

  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";

class dynamicLocale {
    // Class for changing language in a website
    // language_input_selector (optional) : selector for hidden input
    // whose value is the locale of the page
    
    // child classes must define a method locale_path which receives a
    // locale as a string and returns the path to a json file with
    // locales compatible with gettext.js
    
    // child classes should call declare_available_locales in their
    // constructor method to set up the available locales and display names;

    // If the child class provides a method on_language_change(), it
    // will be called after a language is called.
    
    constructor(language_input_selector=null,default_locale="en"){
	
	this.i18n=window.i18n();
	this.display_map = new Map();
	this.loaded_locales=[default_locale];
	this.finished_loading=this.read_start_locale(language_input_selector);
	this.finished_loading.then(() => this.display_messages());

    }

    init_definitions(){
	;
    }
    
    async read_start_locale(language_input_selector){
	if(language_input_selector != null){
	    var load_locale=document.querySelector(language_input_selector).value;	
	    await this.load_locale(load_locale,true);
	}else{
	    this.init_definitions();
	}
	
    }

    async change_locale(locale){
	// Changes to a loaded locale
	this.i18n.setLocale(locale);
	this.display_messages();	
	return 1;
    }

    async load_locale(locale,init=false){
	// If locale has not beed loaded before it is read and the
	// website switches to that language.
	if ( ! this.loaded_locales.includes(locale) ){
	    await fetch(this.locale_path(locale)).
		then( x  => x.json()).
		then( x => {
		    this.i18n.loadJSON(x);
		    this.loaded_locales.push(locale);
		    if(init)
			return this.change_locale(locale).then(()=>{this.init_definitions();});
		    else
			return this.change_locale(locale);
		});
	}else{
	    if(init)
		return this.change_locale(locale).then(()=>{this.init_definitions();});
	    else
		return this.change_locale(locale);	    
	}
    }

    add_display_message(element,thunk,show=false){	
	if(show){
	    element.innerHTML=thunk();
	}
	this.display_map.set(element,thunk);
    }

    display_messages(){
	this.display_map.forEach((value,key,map) => {
	    key.innerHTML = value();
	});
    }

    declare_available_locales(locales_dictionary){
	this.available_locales=locales_dictionary;
    }
    
    define_language_selector(selector=null){
	//It should be called after declare_available_locales
	
	// Returns a selector element whose options are the available
	// locales.

	// If selector is null a new selector element is created. If
	// it is a string it should be an id selector of a select
	// element defined in the html, in that case the select
	// element options are populated based on the available
	// locales. Finally it can be a select element and in that
	// case its options are populated with the available locales.
	
	if(selector == null){
	    this.language_selector=document.createElement("select");
	}else if(typeof(selector) == "string"){
	    this.language_selector=document.querySelector(selector);
	}else if(typeof(selector) == "object" && selector.nodeName == "SELECT"){
	    this.language_selector=selector;
	}else{
	    throw new Error("selector should either be null or an id query or a select node");
	}

	var current_locale = this.i18n.getLocale();
	function create_option(text,value){
	    var option = document.createElement("option");
	    option.text=text;
	    option.value=value;
	    if(value == current_locale){
		option.selected = true;
	    }
	    return option;
	}

	if(this.language_selector != null) {
	    Object.keys(this.available_locales).forEach( x =>{
		this.language_selector.add(create_option(this.available_locales[x],x));
	    });

	    this.language_selector.onchange= new_locale => {		
		Promise.resolve(this.on_language_change()).then(
		    () => {this.load_locale(this.language_selector.value);}
		);
		
	    };
	}
    }

    on_language_change(){
	;
    }
    
    
}
